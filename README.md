# README #

1/3スケールのNEC PC-8801風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1981年11月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-8800%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801/raw/2b09ee683d4c3849bb5690bb511fa5bf69ba2b96/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801/raw/2b09ee683d4c3849bb5690bb511fa5bf69ba2b96/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801/raw/2b09ee683d4c3849bb5690bb511fa5bf69ba2b96/ExampleImage.jpg)
